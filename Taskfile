#!/usr/bin/env bash

set -e

PATH=./bin:./vendor/bin:$PATH

# Import .env file as environment variables
[ -f .env ] && export $(grep -v '^#' .env | xargs -d '\n')


function default {
  it
}

function it {
  echo "I'm runnint!"
}

function jekyll {
  docker run --rm \
    --volume="$PWD/website:/srv/jekyll" \
    --volume="$PWD/vendor/bundle:/usr/local/bundle" \
    -it jekyll/jekyll:$JEKYLL_VERSION \
    jekyll ${@}
}

function jekyll:serve {
  docker run --rm \
    --volume="$PWD/website:/srv/jekyll" \
    --volume="$PWD/vendor/bundle:/usr/local/bundle" \
    -p 4000:4000 \
    -it jekyll/jekyll:$JEKYLL_VERSION \
    jekyll serve ${@}
}

function jekyll:build {
  docker run --rm \
    --volume="$PWD/website:/srv/jekyll" \
    --volume="$PWD/vendor/bundle:/usr/local/bundle" \
    -it jekyll/jekyll:$JEKYLL_VERSION \
    jekyll build ${@}
}

function webserver:build {
  docker build -t $IMAGE_NAME:$IMAGE_TAG .
}

function webserver:push {
  docker push $IMAGE_NAME:$IMAGE_TAG
}

function webserver:build-push {
  webserver:build
  webserver:push
}

function webserver:run {
  docker run -d --rm -p 8080:80 $IMAGE_NAME:$IMAGE_TAG
  echo "http://localhost:8080/"
}

function webserver:stop {
  docker stop $(docker ps -q --filter ancestor=$IMAGE_NAME )
}

function deploy {
  ssh -t $DEPLOYMENT_USER@$VPS "rm -rf /home/$DEPLOYMENT_USER/stack/$APP_NAME"
  ssh -t $DEPLOYMENT_USER@$VPS "mkdir -p /home/$DEPLOYMENT_USER/stack/$APP_NAME"
  scp .envrc $DEPLOYMENT_USER@$VPS:/home/$DEPLOYMENT_USER/stack/$APP_NAME/.envrc
  scp docker-compose.stack.yml $DEPLOYMENT_USER@$VPS:/home/$DEPLOYMENT_USER/stack/$APP_NAME/docker-compose.stack.yml
  ssh -t $DEPLOYMENT_USER@$VPS "direnv allow /home/$DEPLOYMENT_USER/stack/$APP_NAME"
  ssh -t $DEPLOYMENT_USER@$VPS "cd /home/$DEPLOYMENT_USER/stack/$APP_NAME/ ; direnv exec ./ docker stack deploy -c docker-compose.stack.yml $APP_NAME --with-registry-auth"
}

function help {
  echo "$0 <task> <args>"
  echo "Tasks:"
  compgen -A function | cat -n
}

TIMEFORMAT="Task completed in %3lR"
time ${@:-default}

# Unset all environment variables from .env file loaded at beginning
[ -f .env ] && unset $(grep -v '^#' .env | sed -E 's/(.*)=.*/\1/' | xargs)

unset PATH
