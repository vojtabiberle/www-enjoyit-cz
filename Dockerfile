FROM caddy:2-alpine

COPY webserver/caddy/config/Caddyfile /etc/caddy/Caddyfile
COPY website/_site /srv